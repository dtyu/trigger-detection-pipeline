# README #

This repo includes the end-to-end trigger detection pipeline. Our Pipeline contains three steps: pixel clusterin, track reconstruction and trigger detection. All the code and config settings can be found in sPHENIX folder.

## sPHENIX/parsers

This folder contains the parser files which can be used to convert JSON files describing the events into individual '.npz' files that are used by the PyTorch models for training and evaluation.

- `parser_INTT_clustered.py`: This scripts produces 'hits' files. Pixels are clustered into individual hits, and a graph is constructed on the hits using heuristics. The ground-truth edges are labeled. Additional information, such as the momentum, particle ID, etc of the particle that let each hit is included.
- `parser_trkvec_marksignal.py`: This script produces 'track' files. Pixels are clustered into individual hits, and then these hits are grouped into track. A track is the 5 hits, one for each layer in the detector. If a particular track doesn't have a hit on a layer, the 'hit' for that layer is just `(0, 0, 0)`. A cricle is fit onto the track to estimate the track radius. Additional attributes, such as the momentum, particle ID, etc of the particle that generated the track is stored.

The `DATA_DIR` variable in each of the preceding scripts must be modified to specify the location of the input JSON files.

## sPHENIX/tracking-GNN

This folder contains the model and training code for the tracking model. The tracking model takes hits files as input. The hits files contain an overconnected graph constructed on the hits. The tracking model filters out the overconnected edges, keeping only the true edges. True edges are those that are between hits created by the same particle.

- `train.py`: This file contains the training and evaluation code for the tracking models. It reads `configs/tracking.yaml` One can also pass a different config on the command line with the --config argument. Training proceeds as specified in the config `yaml` file. When running the script, the location of where model checkpoints are saved is logged on `stdout`.
- `tracking.yaml`: This is the default config file used by the `train.py` script. 

## Numpy Fields
We first cluster adjacent activated pixels into a single 'hit'. This reduces the amount of data we have while still being a faithful representation of the event. 

- `hits`: Coordinates of hits and the number of pixels activated. This is a 4d vector with entries (r, φ, z, n_pixels). The first three hits are the coordinates of the hit in cylyndirical coordinates.

- `scaled_hits`: Coordinates of hits and the number of pixels activated, with some features scaled. This is a 4d vector with entries (r/3, φ, z/3, n_pixels). The first three hits are the coordinates of the hit in cylyndirical coordinates, with the r and z entries divided by 3.

- `layer_id`: This is the layer on which the hit resides

- `pid`. This is the ID of the particle that created the hit. When the pixels lit up by multiple particles are grouped together into a single hit (which can happen, though infrequently), the mode PID of each pixel is used. For examples, we have 3 pixels lit up that are all adjacent with PID (2, 2, 3). The PID of the grouped hit is 2.

- `hits_xyz`: The X, Y, and Z coordinate of the hit.

- `p_momentum`: The momentum of the particle that created the hit.

- `ip`: The interaction point of the event.

- `noise_label`: Indicate hits that come from pixels that were not activated by any particles.

- `ParticleTypeID`: Specifies the type of particle that created each hit. If pixels from multiple particles are clustered into a single hit, the mode ParticleTypeID is used.

- `is_complete_trk`: Specifies whether the hit belongs to a track that has at least one hit in the MVTX layers and one hit in the INTT layers.

- `trigger_track_flag`: Specifies whether the hit belongs to a trigger track.

- `trigger`: Specifies whether the event is a trigger event.

- `valid_trigger_flag`: Specifies whether at least pair of trigger tracks was found was captured by the detector (regardless of whether these tracks were crated). Some events labeled as trigger events may not actually be valid trigger events because one or more of the trigger tracks did not actually go through the MVTX or INTT regions.


## Standardized Numpy Fields
- `hit_cartesian`: coordinate of hits in cartesian coordinates. Shape (n_hits, 3).
- `hit_cylindrical`: coordinates of hits in cylindrical coordinates. Shape (n_hits, 3). No longer scaled! We need to update the tracking model to reflect this (should be r/3, phi, z/3)
- `layer_id`: layer id of the hit. Shape (n_hits). 
- `n_pixels`: number of pixels of the hit. Shape (n_hits)
- `energy`: energy of the hit/track. NaN for noise hits. Shape (n).
- `momentum`: momentum of the hit/track. NaN for noise hits. Shape (n, 3)
- `interaction_point`: interaction point of the event. Shape (3)
- `trigger`: labels whether the event is a trigger event or not. Shape (0)
- `has_trigger_pair`: labels whether the event has a pair of trigger tracks or not.
- `track_origin`: coordinate of the track origin in cartesian coordinates. Shape (n, 3). NaN for noise hits.
- `edge_index` : list of edges. Shape (2, n_edges)
- `edge_z0` : the projected z_0 coordinate of the edge. Shape (n_edges)
- `edge_phi_slope`: the phi slope of an edge. Shape (n_edges)
- `phi_slope_max`: the maximum phi slope used to determine the edge candidates. Shape (1)
- `z0_max`: the maximum z_0 used to determine the edge candidates. Shape (1)
- `trigger_node`: specifies whether the hit/track came from a trigger particle. Shape (n)
- `particle_id`: id of particle that created hit/track. NaN for noise hits. Shape (n)
- `particle_type`: type of particle that created hit/track. NaN for noise hits.
- `parent_particle_type`: forthcoming. Will contain the type of the parent particle.
- `track_hits`: hits that belong to the track, per layer. Missing hits are 0. Shape (n_tracks, 15)
- `track_n_hits`: number of hits that belong to the track. 
- `edge_confidence`: Confidence of the model that the edge is a true edge.

# Removed:
- `is_complete_trk`
- `noise_label`


#np.savez(output_dir_event, hits=hits, scaled_hits=scaled_hits, hits_xyz=hits_xyz, noise_label=y, valid_trigger_flag=valid_trigger_flag, layer_id=np.array(hits_df['layer_id']),edge_index=edge_index, pid=pid, n_hits=n_hits, n_tracks=n_tracks, dphi=dphi, z_slopes=z_slopes, trigger=trigger, ip=ip, psv=np.array(hits_df['psv']), p_momentum=np.array(hits_df['p_momentum']), ParticleTypeID=np.array(hits_df['ParticleTypeID']), is_complete_trk=np.array(hits_df['is_complete_trk']), trigger_track_flag=np.array(hits_df['TriggerTrackFlag']))
    

